# Laravel Nova

## Installation steps

-   Add the following snippet to your `composer.json` file.

```json
"repositories": [
   {
       "type": "vcs",
       "url": "git@git.donatix.info:packages/nova.git"
   }
],
```

-   Run `composer require laravel/nova` to install it.
-   Now you are ready to continue reading the [official docs](https://nova.laravel.com/docs/1.0/installation.html#installing-nova).
-   Otherwise you can run `php artisan nova:install` and `php artisan migrate` and head to `/nova` of your app to start playing around with it.

## Get new versions

Search in github for `NovaCoreServiceProvider` or some other nova related term and apply filters `Code` and `php`.
Last time I used `https://github.com/nvaware/nva`.
You might check `Nova.php -> version()` for version of the package.
